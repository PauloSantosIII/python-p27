---Todas as tasks do grupo com nome "Rotina de Estudos"
SELECT * FROM tasks t
	JOIN groups g ON g.id = t.groupid
		WHERE g.name = 'Rotina de Estudos';

---O nome de cada task e o nome do grupo o qual é pertencente (mesmo que uma task não esteja relacionada a um grupo, ela deve ser retornada)
SELECT t.name , g.name FROM tasks t
	LEFT OUTER JOIN groups G ON g.id = t.groupid;

---O nome de cada task a qual não se relaciona com nenhum grupo
SELECT t.name FROM tasks t
	WHERE t.groupid IS NULL;

---As tasks do grupo com nome "Rotina de Casa" com prioridade maior que 3
SELECT t.name FROM tasks t
	JOIN GROUPS g ON g.id = t.groupid 
		WHERE g.name = 'Rotina de Casa' 
			AND t.priority > 3;

---As tasks do grupo com nome "Rotina de Estudos" ou "Rotina de atividade física" com prioridade a partir de 2
SELECT t.name FROM tasks t
	JOIN GROUPS g ON g.id = t.groupid 
		WHERE g.name = 'Rotina de Estudos' OR g.name = 'Rotina de atividade física'
			AND t.priority >= 2;

---As tasks com duração menor que 30 minutos que se relaciona com o grupo "Rotina de Casa"
SELECT t.name FROM tasks t
	JOIN GROUPS g ON g.id = t.groupid 
		WHERE g.name = 'Rotina de Casa'
			AND t.duration < 30;

---As tasks que não se relacionam com nenhum grupo e tem duração inferior a 30 minutos
SELECT t.name FROM tasks t
	WHERE t.duration < 30;
